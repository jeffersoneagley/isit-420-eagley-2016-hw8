
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 03/12/2017 15:25:31
-- Generated from EDMX file: D:\Users\fish\OneDrive\Documents\Visual Studio 2015\Projects\ISIT420\HW8 game\HW8-game-eagley-kasner\HW8-game-eagley-kasner\ModelHighscore.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BreakoutHighscoreDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[HighScores]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HighScores];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'HighScores'
CREATE TABLE [dbo].[HighScores] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Points] int  NOT NULL,
    [WavesCompleted] nvarchar(max)  NOT NULL,
    [PlayerName] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'HighScores'
ALTER TABLE [dbo].[HighScores]
ADD CONSTRAINT [PK_HighScores]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------