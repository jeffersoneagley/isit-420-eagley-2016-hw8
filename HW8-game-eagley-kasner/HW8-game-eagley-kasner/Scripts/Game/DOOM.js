﻿var checkForGameOver = function (playerName) {
    function ballIsInMap(transform) {
        return transform.x >= 0 &&
            transform.y >= 0 &&
            transform.x <= myCanvas.width &&
            transform.y <= myCanvas.height;
    }
    for (ball in cartographer.balls) {
        if (ballIsInMap(cartographer.balls[ball].transform)) {
            return;
        }
    }
    Animator.isRunningGame = false;
    Animator.animTimeout = null;

    //end game
    var result = {
        "score": scoreboard.score, "playerName": playerName, "wavesCompleted": scoreboard.levelsCompleted
    };
    $.post('/Game/GameOver', result, function (data) {
        $('body').html(data);
    })
}