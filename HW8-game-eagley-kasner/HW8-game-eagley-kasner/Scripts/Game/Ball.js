﻿//define ball 
var makeBall = function(startX, startY) {
    var ball = {
        transform: Transform(),
        onCollisionEnter: function (collision) {
            //get direction of collision
            //collision.collider.transform
        },
        draw: function (ctx) {
            ctx.fillStyle = "Cyan";
            ctx.fillRect(ball.transform.x, ball.transform.y, ball.transform.width, ball.transform.height);
        }
    }
    ball.transform.x = startX || 50;
    ball.transform.y = startY || 120;
    ball.transform.width = 15;
    ball.transform.height = ball.transform.width;

    ball.transform.velocity.angle = Math.PI/2;
    ball.transform.velocity.speed = 50;
    return ball;
}