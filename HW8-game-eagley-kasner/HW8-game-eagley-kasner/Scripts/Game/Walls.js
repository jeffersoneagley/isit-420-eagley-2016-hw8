﻿//define wall 
var makeWall = function (startX, startY, width, height) {
    var wall = {
        transform: Transform(),
        draw: function (ctx) {
            ctx.strokeStyle = '3px RGBA(128,128,128,0.80)';
            ctx.fillStyle = "yellow";
            ctx.fillRect(wall.transform.x, wall.transform.y, wall.transform.width, wall.transform.height);
            ctx.strokeRect(wall.transform.x, wall.transform.y, wall.transform.width, wall.transform.height);
        }
    };
    wall.transform.x = startX || 0;
    wall.transform.y = startY || 0;
    wall.transform.width = width || 20;
    wall.transform.height = height || 100;

    wall.transform.velocity.angle = 0;
    wall.transform.velocity.speed = 0;

    return wall;
}

var defaultWalls = function () {
    var left = makeWall(-5, 0, 35, myCanvas.height);
    var right = makeWall(myCanvas.width - 30, 0, 35, myCanvas.height);
    var end = makeWall(0, myCanvas.height - 30, myCanvas.width, 30);
    cartographer.walls.push(left, right, end);
    physics.staticCollisionItems.push(left, right, end);
    Animator.renderItems.push(left, right, end);
}