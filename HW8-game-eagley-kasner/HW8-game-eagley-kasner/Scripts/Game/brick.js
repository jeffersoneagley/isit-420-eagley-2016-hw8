﻿//define brick maker
var makeBrick = function (width, height) {
    var brick = {
        isColliding: false,
        isDead: false,
        transform: Transform(),
        draw: function (ctx) {
            ctx.strokeStyle = '3px RGBA(128,128,128,0.80)';
            ctx.fillStyle = "forestgreen";
            ctx.fillRect(brick.transform.x, brick.transform.y, brick.transform.width, brick.transform.height);
            ctx.strokeRect(brick.transform.x, brick.transform.y, brick.transform.width, brick.transform.height);
        }
    };
    brick.transform.width = width || 15;
    brick.transform.height = height || 10;

    brick.transform.onCollision = function () {
        if (!brick.isColliding) {
            brick.isColliding = true;
            scoreboard.onScore();
            brick.isDead = true;
            somethingDied = true;
            cartographer.killBrick(brick);
        }

    };
    return brick;
};