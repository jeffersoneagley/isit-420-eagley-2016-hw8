﻿var makePhysics = function () {
    //phys
    var physicsObject = {
        deltaTime: 0,
        lastTime: Date.now(),
        collisionItems: [],
        staticCollisionItems: [],
        areColliding: function (objectA, objectB) {
            var overlap = {
                x: objectA.transform.velocity.nextPosition().x < objectB.transform.velocity.nextPosition().x + objectB.transform.width &&
                objectA.transform.velocity.nextPosition().x + objectA.transform.width > objectB.transform.velocity.nextPosition().x,

                y: objectA.transform.velocity.nextPosition().y < objectB.transform.velocity.nextPosition().y + objectB.transform.height &&
                objectA.transform.height + objectA.transform.velocity.nextPosition().y > objectB.transform.velocity.nextPosition().y
            };
            return overlap.x && overlap.y;
        },

        isPhysicsStaticOrColliderTick: false,
        reflectEquation: function () {
            return Math.PI + (Math.random() - 0.5);
        },
        checkCollisions: function () {
            //alternate between statics and mobiles to save on phys processing each tick
            if (physicsObject.isPhysicsStaticOrColliderTick) {
                for (itemA in physicsObject.collisionItems) {
                    for (itemB in physicsObject.collisionItems) {
                        if (
                            (physicsObject.collisionItems[itemA] != physicsObject.collisionItems[itemB]) &&
                            physicsObject.areColliding(physicsObject.collisionItems[itemA], physicsObject.collisionItems[itemB])
                        ) {
                            physicsObject.collisionItems[itemA].transform.velocity.angle += Math.PI * physicsObject.reflectEquation();
                            var next = physicsObject.collisionItems[itemA].transform.velocity.nextPosition();
                            physicsObject.collisionItems[itemA].transform.x = next.x;
                            physicsObject.collisionItems[itemA].transform.y = next.y;
                            if (physicsObject.collisionItems[itemA].transform.onCollision) {
                                physicsObject.collisionItems[itemA].transform.onCollision();
                            }
                            if (physicsObject.collisionItems[itemB].onCollision && !physicsObject.collisionItems[itemB].isColliding) {
                                physicsObject.collisionItems[itemB].onCollision();
                            }
                        }
                    }
                    for (itemB in physicsObject.staticCollisionItems) {
                        if (physicsObject.areColliding(physicsObject.collisionItems[itemA], physicsObject.staticCollisionItems[itemB])) {
                            physicsObject.collisionItems[itemA].transform.velocity.angle += Math.PI * physicsObject.reflectEquation();
                        }
                        if (physicsObject.staticCollisionItems[itemB].onCollision && !physicsObject.staticCollisionItems[itemB].isColliding) {
                            physicsObject.staticCollisionItems[itemB].onCollision();
                        }
                    }
                }
            } 
            physicsObject.isPhysicsStaticOrColliderTick = !physicsObject.isPhysicsStaticOrColliderTick;
        },
        movePhysicsObjects: function () {
            physicsObject.checkCollisions();
            for (item in physicsObject.collisionItems) {
                if (physicsObject.collisionItems[item].transform && physicsObject.collisionItems[item].transform.refresh) {
                    physicsObject.collisionItems[item].transform.refresh();
                }
            }
            for (item in physicsObject.staticCollisionItems) {
                if (physicsObject.staticCollisionItems[item].transform && physicsObject.staticCollisionItems[item].transform.refresh) {
                    physicsObject.staticCollisionItems[item].transform.refresh();
                }
            }
        },
        calculateDelta: function () {
            physicsObject.deltaTime = Date.now() - physicsObject.lastTime;
            physicsObject.lastTime = Date.now();
        }
    }
    return physicsObject;
}