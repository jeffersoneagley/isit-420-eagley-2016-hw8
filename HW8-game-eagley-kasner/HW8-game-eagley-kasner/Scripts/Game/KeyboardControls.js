﻿var KeyboardControls = function () {
    var keyboardController = {
        onLeft: [],
        onRight: [],
        onKeysUp: [],
        bind: function () {
            window.addEventListener("keydown", function (event) {
                if (event.defaultPrevented) {
                    return; // Do nothing if the event was already processed
                }
                var keyCode = event.keyCode;
                switch (keyCode) {
                    case 68: //d
                        for (thing in keyboardController.onRight) {
                            keyboardController.onRight[thing]();
                        }
                        break;
                    case 65: //a
                        for (thing in keyboardController.onLeft) {
                            keyboardController.onLeft[thing]();
                        }
                        break;

                }

                // Cancel the default action to avoid it being handled twice
                event.preventDefault();
            }, true);


            window.addEventListener("keyup", function (event) {
                if (event.defaultPrevented) {
                    return; // Do nothing if the event was already processed
                }
                var keyCode = event.keyCode;
                switch (keyCode) {
                    case 68: //d
                    case 65: //a
                        for (thing in keyboardController.onKeysUp) {
                            keyboardController.onKeysUp[thing]();
                        }
                        break;
                }

                // Cancel the default action to avoid it being handled twice
                event.preventDefault();
            }, true);


        }
    }
    return keyboardController;
}