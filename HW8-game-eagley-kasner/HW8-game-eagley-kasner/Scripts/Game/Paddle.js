﻿var makePlayerPaddle = function(startX, startY) {
    var paddle = {
        speed:100,
        transform: Transform(),
        draw: function (ctx) {
            ctx.strokeStyle = '5px yellow';
            ctx.fillStyle = "gold";
            ctx.fillRect(paddle.transform.x, paddle.transform.y, paddle.transform.width, paddle.transform.height);
            ctx.strokeRect(paddle.transform.x, paddle.transform.y, paddle.transform.width, paddle.transform.height);
        },
        keyboard: KeyboardControls()
    };
    paddle.transform.x = startX || 15;
    paddle.transform.y = startY || 10;

    paddle.transform.width = 45;
    paddle.transform.height = 30;

    paddle.keyboard.onLeft.push(function () {
        paddle.transform.velocity.speed = -paddle.speed;
    });
    paddle.keyboard.onRight.push(function () {
        paddle.transform.velocity.speed = paddle.speed;
    });
    paddle.keyboard.onKeysUp.push(function () {
        paddle.transform.velocity.speed = 0;
    });
    paddle.keyboard.bind();

    physics.staticCollisionItems.push(paddle);
    Animator.renderItems.push(paddle);
    return paddle;
}