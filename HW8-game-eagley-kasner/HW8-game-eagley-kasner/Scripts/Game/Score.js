﻿var scoreboard = {
    score: 0,
    levelsCompleted: 0,
    onScore: function () {
        checkForGameOver("@Model.PlayerName");
        scoreboard.score++;
        $("#gameScore").html(scoreboard.score + " points!~");
        for (var ball in cartographer.balls) {
            if (cartographer.balls[ball] && cartographer.balls[ball].transform && cartographer.balls[ball].transform.velocity) {
                cartographer.balls[ball].transform.velocity.speed *= 1.01;
            }
        }
    }
}