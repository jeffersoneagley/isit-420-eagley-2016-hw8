﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HW8_game_eagley_kasner;

namespace HW8_game_eagley_kasner.Controllers
{
    public class HighScoresController : Controller
    {
        private ModelHighscoreContainer db = new ModelHighscoreContainer();

        // GET: HighScores
        public ActionResult Index()
        {
            return View(db.HighScores.ToList());
        }

        // GET: HighScores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HighScore highScore = db.HighScores.Find(id);
            if (highScore == null)
            {
                return HttpNotFound();
            }
            return View(highScore);
        }

        // GET: HighScores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HighScores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Points,WavesCompleted,PlayerName")] HighScore highScore)
        {
            if (ModelState.IsValid)
            {
                db.HighScores.Add(highScore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(highScore);
        }

        // GET: HighScores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HighScore highScore = db.HighScores.Find(id);
            if (highScore == null)
            {
                return HttpNotFound();
            }
            return View(highScore);
        }

        // POST: HighScores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Points,WavesCompleted,PlayerName")] HighScore highScore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(highScore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(highScore);
        }

        // GET: HighScores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HighScore highScore = db.HighScores.Find(id);
            if (highScore == null)
            {
                return HttpNotFound();
            }
            return View(highScore);
        }

        // POST: HighScores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HighScore highScore = db.HighScores.Find(id);
            db.HighScores.Remove(highScore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
