﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW8_game_eagley_kasner.Controllers
{
    public class GameController : Controller
    {
        private ModelHighscoreContainer db = new ModelHighscoreContainer();

        // GET: Game
        public ActionResult Index(string playerName)
        {
            HighScore currentPlayer = new HighScore();
            currentPlayer.PlayerName = playerName;
            if (playerName != null)
            {
                return View(currentPlayer);
            }
            else
            {
                return RedirectToRoute("Home");
            }

        }

        public ActionResult GameOver(string playerName, int score, int wavesCompleted)
        {
            HighScore ply = new HighScore();
            ply.PlayerName = playerName;
            ply.Points = score;
            ply.WavesCompleted = wavesCompleted.ToString();//made a string due do db mistake, could fix, but not going to

            db.HighScores.Add(ply);
            db.SaveChanges();

            return View(ply);
        }
    }
}