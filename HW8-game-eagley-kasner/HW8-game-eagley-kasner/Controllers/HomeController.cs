﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW8_game_eagley_kasner.Controllers
{
    public class HomeController : Controller
    {
        private ModelHighscoreContainer db = new ModelHighscoreContainer();

        // GET: HighScores
        public ActionResult Index()
        {
            return View(db.HighScores.OrderByDescending(w=>w.Points).ToList());
        }
    }
}