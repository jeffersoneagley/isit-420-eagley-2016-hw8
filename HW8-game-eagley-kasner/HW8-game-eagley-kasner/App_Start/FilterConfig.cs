﻿using System.Web;
using System.Web.Mvc;

namespace HW8_game_eagley_kasner
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
